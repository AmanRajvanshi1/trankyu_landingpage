import React, { Component } from "react";
import CallIcon from "@mui/icons-material/Call";
import KeyboardArrowDownOutlinedIcon from "@mui/icons-material/KeyboardArrowDownOutlined";
import "./header.css";
import Navbar from "./Navbar";

class Header extends Component {
  render() {
    return (
      <>
        <div>
          <div className="top-address-div">
            <div className="left-info">
              <img src="/images/Phone.svg" />
              <div className="contact_content">
                <h5>Appeler à l'aide</h5>
                <h3>(+228)90 80 91 91</h3>
              </div>
            </div>
            <div className="right_header">
              <div className="right_header_start">
                <h3>French</h3>
                <KeyboardArrowDownOutlinedIcon />
              </div>
              <div className="right_header_end">
                <h3>Togo Lome</h3>
                <KeyboardArrowDownOutlinedIcon />
              </div>
            </div>
          </div>
          <div className="navbar-container">
            <Navbar />
          </div>
        </div>
      </>
    );
  }
}

export default Header;
