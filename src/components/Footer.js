import React, { Component } from "react";
import "./footer.css";
import DoubleArrowTwoToneIcon from "@mui/icons-material/DoubleArrowTwoTone";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
export class Footer extends Component {
  render() {
    return (
      <div className="footer-container">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="left-div">
                <img src="/images/Group 1393.svg" />
                <p>
                  Trankyl is the only application for your works and services in
                  Togo: household, Repair, Carpentry, Courses, Beauty,
                  Hairdressing and others.
                </p>
                <div className="social-media-logo-div">
                  <img src="images/Group 1397.svg" />
                  <img src="images/Group 1394.svg" />
                  <img src="images/Group 1395.svg" />
                  <img src="images/Group 1398.svg" />
                  <img src="images/Group 1396.svg" />
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="middle-div">
                <h4>Quick Links</h4>
                <ul>
                  <li>
                    <a href="#" className="quick_links">
                      <img src="/images/Path 3577.svg" alt="" />
                      <img src="/images/Path 3577.svg" alt="" />
                      <p>Mon Compte</p>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="quick_links">
                      <img src="/images/Path 3577.svg" alt="" />
                      <img src="/images/Path 3577.svg" alt="" />
                      <p>Inscription</p>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="quick_links">
                      <img src="/images/Path 3577.svg" alt="" />
                      <img src="/images/Path 3577.svg" alt="" />
                      <p>Politique de confidentialité</p>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="quick_links">
                      <img src="/images/Path 3577.svg" alt="" />
                      <img src="/images/Path 3577.svg" alt="" />
                      <p>Termes et conditions</p>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-4">
              <div className="right-div">
                <h4>Contact</h4>
                <ul>
                  <li>
                    <img src="/images/Group 1399.svg" alt="" style={{marginRight:"10px"}}/>
                    <p>
                      Amadahome' district, near CAPAmadahome' station, Lome'
                      -Togo
                    </p>
                  </li>
                  <li>
                    <img src="/images/Group 1400.svg" alt="" style={{marginRight:"10px"}}/>
                    <div className="d-flex flex-column"><a href="">
                      <p>+228 92 11 90 88</p>
                    </a>
                    <a href="">
                      <p>+228 90 80 91 91</p>
                    </a></div>
                  </li>
                  <li>
                    <img src="/images/Path 3566.svg" alt="" style={{marginRight:"10px"}}/></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="row bottom_row">
          <p>Trankyl 2021 All rights reserved.</p>
        </div>
      </div>
    );
  }
}

export default Footer;
