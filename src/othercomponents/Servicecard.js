import { height } from "@mui/system";
import React, { Component } from "react";
import "./servicecard.css";
export class Servicecard extends Component {
  render() {
    return (
      <div className="servicecard-container">
        <div className="card">
          <img src="/images/card.jpeg" className="card-img-top" alt="..." />
          <div className="card-body">
            <p className="card-text">Couse/Shopping</p>
          </div>
        </div>
        <div className="card">
          <img src="/images/card.jpeg" className="card-img-top" alt="..." />
          <div className="card-body">
            <p className="card-text">Couse/Shopping</p>
          </div>
        </div>

        <div className="card">
          <img src="/images/card.jpeg" className="card-img-top" alt="..." />
          <div className="card-body">
            <p className="card-text">Couse/Shopping</p>
          </div>
        </div>
        <div className="card">
          <img src="/images/card.jpeg" className="card-img-top" alt="..." />
          <div className="card-body">
            <p className="card-text">Couse/Shopping</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Servicecard;
