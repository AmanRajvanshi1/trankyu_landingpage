import React, { Component } from "react";
import "./servicesidelist.css";
import ArrowForwardIosOutlinedIcon from "@mui/icons-material/ArrowForwardIosOutlined";
export class Servicesidelist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }
  render() {
    return (
      <div
        className="servicesidelist-container"
        style={{ overflowY: "scroll" }}
      >
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
        <div
          class="list-group"
          onClick={() => this.setState({ isLoading: true })}
        >
          <a href="#" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between align-items-center" >
              <div className="d-flex align-items-center">
              <img src="/images/Group 1261.svg" className="side_img" />
              <h3 class="mb-1 w-100">Course</h3>
              </div>
              <ArrowForwardIosOutlinedIcon />
            </div>
          </a>
        </div>
      </div>
    );
  }
}

export default Servicesidelist;
