import React, { Component } from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "./middlecarousel.css";

const options = {
  responsiveClass: true,
  autoplay: true,
  smartSpeed: 500,
  dots:true,
  loop: true,
  responsive: {
    0: {
      items: 1,
    },
    400: {
      items: 1,
    },
    600: {
      items: 1,
    },
    700: {
      items: 1,
    },
    1000: {
      items: 4,
    },
  },
};
export class MiddleCarousel extends Component {
  
  render() {
    return (
      <>
        <div className="middle-carousel-div">
          <div className="main-heading">
            <h4 style={{ color: "white", fontSize: "20px" }}>Nos Clients</h4>
            <h3 style={{ color: "white", fontWeight: "600" }}>
              ils parlent de trankyl
            </h3>
          </div>
          <div className="carousel-images">
            <OwlCarousel {...options}>
              <img src="/images/Group 1415@2x.png" className="image_carousel"/>
              <img src="/images/Screen-Shot-2021-12-18-at-10.10.11-PM@2x.png" className="image_carousel"/>
              <img src="/images/Screen-Shot-2021-07-12-at-12.00.08-AM.png" className="image_carousel"/>
              <img src="images/Togo-first.png" className="image_carousel"/>
            </OwlCarousel>
          </div>
        </div>
      </>
    );
  }
}

export default MiddleCarousel;
