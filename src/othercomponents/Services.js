import React, { Component } from "react";
import Servicecard from "./Servicecard";
import "./services.css";
import Servicesidelist from "./Servicesidelist";
export class Services extends Component {
  render() {
    return (
      <div className="services-container">
        <div className="row" style={{textAlign:"center"}}>
        <h4>Nos Services</h4>
        <h2>Nous offrons un large Variété de services</h2>
        </div>
        <div className="container">
          <div className="row d-flex align-items-center">
            <div className="col-md-6">
              <Servicesidelist />
            </div>
            <div className="col-md-6">
              <Servicecard />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Services;
