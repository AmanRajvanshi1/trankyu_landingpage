import React, { Component } from "react";
import "./videos.css";

export class Videoportion extends Component {
  render() {
    return (
      <>
        <div className="container">
          <div className="row">
            <div className="col-md-6 video_col">
              <img src="/images/first.png" alt="" className="main_img" />
              <img src="/images/Group 1416.svg" alt="" className="play_img" />
            </div>
            <div className="col-md-6 video_col">
              <img src="/images/second.png" alt="" className="main_img" />
              <img src="/images/Group 1416.svg" alt="" className="play_img" />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Videoportion;
