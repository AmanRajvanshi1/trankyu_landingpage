import React, { Component } from "react";
import "./centergrid.css";

export class Centergrid extends Component {
  render() {
    return (
      <>
        <div className="container">
          <div className="centergrid-container">
            <div className="card">
              <div className="card-body">
                <p className="card-text">Couse/Shopping</p>
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <p className="card-text">Couse/Shopping</p>
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <p className="card-text">Couse/Shopping</p>
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <p className="card-text">Couse/Shopping</p>
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <p className="card-text">Couse/Shopping</p>
              </div>
            </div>
            <div className="card">
              <div className="card-body">
                <p className="card-text">Couse/Shopping</p>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Centergrid;
