import React, { Component } from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'
import Maincarousel from '../components/Maincarousel'
import Centergrid from '../othercomponents/Centergrid'
import MiddleCarousel from '../othercomponents/MiddleCarousel'
import Services from '../othercomponents/Services'
import Videos from '../othercomponents/Videos'

class Home extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Maincarousel/>
        <Services/>
        <MiddleCarousel/>
        <Videos/>
        <Centergrid/>
        <Footer/>
      </div>
    )
  }
}

export default Home